classdef PredictingPerformance < handle
    %PREDICTINGPERFORMANCE Predicting student performance using LMS-data
    %   From data to model, MAT-64010 2019-01 course's Educational data 
    %   mining applications and predicting student performance assignment 
    %   JY01 Merja Halonen
    
    properties
        filename;
        data;
        time;
        count;
        point;
        studentCounts;
        modelKNNScale;
        modelKNNEqual;
        modelKNNScaleAv;
        modelKNNEqualAv;
        modelKNNNorm;
        modelsKNNNormAv;
        modelNBScale;
        modelNBScaleAv;
        modelNBNorm;
        modelsNBNormAv;
        models;
        
        
    end
    
    methods
        function obj = PredictingPerformance(filename,format)
            %PREDICTINGPERFORMANCE Construct of this class
            %   Read and create data and change data time variable to
            %   datatime
            obj.filename = filename;
            obj.data = readtable(obj.filename);
            
            obj.time = datetime(obj.data.Aika,'InputFormat',format);
            
            % Empty cell array for models
            %obj.models = {Scale, Equal};
        end
        
        function dataCounts = ClicksCountPerStudent(obj,column)
            %CLICKCCOUNTPERSTUDENT Calculate clicks counts per student
            %   column = column which is used to calculate amounts per
            %   student
            
            variable = getfield(obj.data, column);
            
            [obj.count,obj.point] = groupcounts(variable);
            dataCounts = zeros(length(obj.point),2);
            dataCounts(:,1) = obj.point;
            dataCounts(:,2) = obj.count;
        end
        
        function dataMat = CalculateDays(obj, table, column, time, matrix)
            %CALCULATEDAYS Calculate days when student has been clicking
            %   table = data table
            %   column = indicates an individual student 
            %   time = days to count
            %   matrix = matrix to compare result matrix size
            
            data = getfield(obj, table);
            variable = getfield(obj.data, column);
            time2 = getfield(obj, time);
            % Gives the rows for a particular student
            obj.studentCounts = length(unique(variable));
            rows = zeros(obj.studentCounts,4);

            % Gets first and last index for student
            [rows(:,1),rows(:,2),ia] = unique(data{:,2},'first');
            [C,rows(:,3),ia] = unique(variable,'last');

            dataMat = zeros(obj.studentCounts,2); 

            for j=1:obj.studentCounts
                dataMat(j,1) = rows(j,1);
                first = rows(j,2);
                last = rows(j,3);
                dataMat(j,3) = length(unique(dateshift(time2(first:last,1),...
                    'start','day')));
            end
            
            % Check if matrix are same size and adds counts of days to
            % dataCounts

            a = sum(sign(dataMat(:,1)-matrix(:,1)));
             if a == 0 
                 dataMat(:,2) = matrix(:,end);
             end
        end
        
        function average = CalculateClicksPerDay(obj,M, col1, col2)
            %CALCULATECLICKSPERDAY  Calculate clicks per day
            %   M = matrix where to do calculation
            %   col1 = data matrix column
            %   col2 = data matrix column
            
            average = M(:,col1)./M(:,col2);
            
        end
        
        function grades = ChangePointsToGrades(obj, matrix, col, range)
            %CHANGEPOINTSTOGRADES Converts points to grades by scaling 
            %   matrix = data matrix
            %   col = data matrix column
            %   range = grades range 
            
            grades = zeros(length(matrix),1);
            grades(:,1) = round(rescale(matrix(:,col),range(1),range(2)));
        end
        
        function gradesEqual = ChangePointsToEqualGrades(obj, matrix,...
                col, nr)
            %CHANGEPOINTSTOEQUALGRADES Converts points to grades by egual
            %   matrix = data matrix
            %   col = data matrix column
            %   range = grades range
            
            % Number of element per each group
            equalGroup = floor(numel(matrix(:,col))/nr);
            
            % Create a group index
            gradesEqual = transpose(repelem((1:nr),equalGroup));
            
        end
        
        function gradesNorm = ChangePointToNormGrades(obj, value, col,...
                matrix, nbins)
            %CHANGEPOINTTONORMGRADES Converts points to grades by normal
            %distribution
            %   value = point for grades < 0
            %   col = matrix column to use
            %   matrix = data matrix
            %   nbins = number of histogram bins = grades scale
            
            
            gradesNorm = zeros(length(matrix),1);
            
            % remove grades with 0 (faild)
            M = matrix;
            idx=find(M(:,col) < value);
            zeroGrades = M(idx,:);
            M(idx,:)=[];
            
            % Create histogram to get limits to grades
            figure;
            x_Label = {'Arvosanat 1-5'};  
            y_Label = {'Lukum��r�'};
            h = histogram(M(:,col),nbins);
            
            xlabel(x_Label);
            ylabel(y_Label);
            
            %grades limits
            edges = h.BinEdges;
            l = length(edges);
            
            for i=1:length(matrix)
                if (matrix(i,col) < value)
                   gradesNorm(i) = 0;
                elseif (matrix(i,col) >= edges(l-1) && matrix(i,col)...
                        < edges(l));
                   gradesNorm(i) = 5;
                elseif (matrix(i,col) >= edges(l-2) && matrix(i,col)...
                        < edges(l-1));
                    gradesNorm(i) = 4;
                elseif (matrix(i,col) >= edges(l-3) && matrix(i,col)...
                        < edges(l-2));
                    gradesNorm(i) = 3;
                elseif (matrix(i,col) >= edges(l-4) && matrix(i,col)...
                        < edges(l-3));
                   gradesNorm(i) = 2;
                elseif (matrix(i,col) >= edges(l-5) && matrix(i,col)...
                        < edges(l-4));
                    gradesNorm(i) = 1;
                end
                
            end
            


        end
        
        function [input, output] = CreateInputOutput(obj, matrix, col1,...
                col2, MatrixOutput, col3)
            %CHANGEPOINTSTOEQUALGRADES Converts points to grades by egual
            %   matrix = data matrix
            %   col1 = data matrix column to input
            %   MatrixOutput = output matrix
            %   col2 = data matrix column to input
            %   col3 = data matrix column to output
            
            input = zeros(length(matrix),2);
            input = matrix(:,col1:col2);
            
            output = zeros(length(MatrixOutput), 1);
            output = MatrixOutput(:,col3);
        end
        
        function scaleValues = MinMaxScaling(obj, input)
            %MINMAXSCALING Do minmax-scaling for matrix
            %   input = data matrix
            
            colmin = min(input);
            colmax = max(input);
            
            %Scaling input to classifier scale
            scaleValues = rescale(input,'InputMin',colmin,'InputMax',...
                colmax);
            
        end 
        
        function ModelsStructure(obj, input, output, N, type) 
            %MODELSSTRUCTURE Create models structure where to put models
            %   input = input data
            %   output = output data
            %   N = number of neighbor
            %   type = to which properties result set 
            
            for j=1:N
                modelKNN(j).model = j;
                modelKNN(j).rloss = j;
                modelKNN(j).kloss = j;
            end
            
            % k-nearest neighbor classifier and quality of KNN
            for i=1:N     
                modelKNN(i).model = obj.KNNmodel(input, output, i); 
                %modelKNN(i).quality = obj.QualityKNN(modelKNN(i).model); 
                Q = obj.QualityKNN(modelKNN(i).model);
                modelKNN(i).rloss = Q.rloss;
                modelKNN(i).kloss = Q.kloss;
            end

            obj = setfield(obj, type, modelKNN);
        end
        
        function model = KNNmodel(obj, input, output, N)
            %KNNMODEL Create k-nearest neighbor classification model
            %   input = input variables
            %   output = output variable
            %   nr = neighbors number
            
            model = fitcknn(input,output,'NumNeighbors',N);
        end
        
        function quality = QualityKNN(obj, model)
            %QUALITYKNN Examine quality of KNN classifier using using 
            %resubstitution and cross validation 
            %   model = model for qualition
            
            % Examine the resubstitution loss
            % Classification error by resubstitution, default the fraction 
            % of misclassified data
            rloss = resubLoss(model);
            
            % Construct a cross-validated classifier from the model
            CVMdl = crossval(model);
            
            % Examine the cross-validation loss
            % Classification loss for observations not used for training
            kloss = kfoldLoss(CVMdl);
            
            quality.rloss = rloss;
            quality.kloss = kloss;
            
        end 
        
        function [label, score, cost] = PredictKNN(obj, model, nr,...
                input, matrix)
            %PREDICTKNN Predict labels using k-nearest neighbor 
            %classification model
            %   model = model for predicting
            %   nr = number of KNN-model
            %   input = input which is the predictor data
            %   matrix = orginal input data for calculate min and max
            
            colmin = min(matrix);
            colmax = max(matrix);
            
            %Scaling input to classifier scale
            sInput = rescale(input,'InputMin',colmin,'InputMax',colmax);
            
            modelName = getfield(obj, model);
            Mdl = modelName(nr).model;
            
            %Predict
            [label, score, cost] = predict(Mdl, sInput);
        end
        
        function [idx, D, neighbors, grades, grade] = SearchKNN(obj,...
                input, newInput, nr, output)
            %SEARCHKNN Find k-nearest neighbors using input data
            %   input = input data
            %   newInput = new data to which find neighbor
            %   nr = number of neighbor
            %   output = output data
            
            [idx,D] = knnsearch(input,newInput,'k',nr);
            
            neighbors = input(idx,:);
            
            [rows,cols] = size(idx);
            grades = zeros(rows,cols);
            grade = zeros(rows,1);
            
            for i=1:rows
                for j=1:cols
                    I = idx(i,j);
                    grades(i,j) = output(I); 
                end
                
                % Most frequent grade in grades. When there are multiple 
                % values occurring equally frequently, mode returns the 
                % smallest of those values.
                % For complex inputs, the smallest value is the first value
                % in a sorted list. 
                grade(i) = mode(grades(i,:));
            end
        end
        
        function histogramFigure(obj, data, x_Label, y_label)
            %HISTOGRAMFIGURE Creates histogram from data
            %   data = input data
            %   x_Label = x label name
            %   y_Label = y label name
            
            figure;
            histogram(data);

            xlabel(x_Label);
            ylabel(y_label);

        end
        
        function scatterFigure(obj, data, colx, coly, x_Label, y_label)
            %SCATTERFIGURE Creates scatter from data
            %   data = input data
            %   colx = column of data for x-axis
            %   coly = cloumn of data for y-axis
            %   x_Label = x label name
            %   y_Label = y label name
            
            figure;
            scatter(data(:,colx), data(:,coly));

            xlabel(x_Label);
            ylabel(y_label);

        end
        
        function ModelsStructureNB(obj, input, output, classNames,...
                distribution, type, percentage, sampleCount) 
            %MODELSSTRUCTURE Create models structure where to put models
            %   input = input data
            %   output = output data
            %   classNames = classes
            %   distribution = distributionNames
            %   type = to which properties result set 
            %   percentage = how much from data is used to test
            %   sampleCount = number of random set of observations in the 
            %   test sample
            
                modelNB.model = 1;
                modelNB.loss = 1;
                modelNB.predictedLabel = 1;

            
            % Naive Bayes classifier and quality of NB
    
                [model, inputTest, outputTest, testIdx] = ...
                    obj.modelNB(input, output, classNames, distribution,...
                    percentage); 
                
                modelNB.model = model;
                
                Q = obj.QualityNB(modelNB.model,inputTest, outputTest);
                modelNB.loss = Q.loss;
                
                S = PredictNBtestSample(obj, model, testIdx, inputTest,...
                    outputTest, sampleCount);
                modelNB.predictedLabel = S;

                obj = setfield(obj, type, modelNB);
        end
        
        function [model, inputTest, outputTest, testInds] = modelNB(obj,...
                input, output, classNames, distribution, percentage)
            %MODELNB Creates naive Bayes model
            %   input = input dta
            %   output = output data
            %   classNames = classes
            %   distribution = distributionNames
            %   percentage = how much from data is used to test

            if isempty(distribution)
                modelPart = fitcnb(input,output,'ClassNames',classNames,...
                    'Holdout',percentage); %Specify a 30% holdout sample 
                                            %for testing
            else
                modelPart = fitcnb(input,output,'ClassNames',classNames,...
                    'Holdout',percentage,'DistributionNames',distribution)
                %Specify a 30% holdout sample for testing
            end
            
            model = modelPart.Trained{1}; % Extract the trained, compact 
                                            % classifier
            testInds = test(modelPart.Partition);% Extract the test indices
            inputTest = input(testInds,:);
            outputTest = output(testInds);
            
        end
        
        function quality = QualityNB(obj, model, inputTest, outputTest)
            %QUALITYNB Examine quality of NB classifier using 
            %classification loss
            %   model = model for qualition
            %   inputTest = holdout input sample for testing
            %   outputTest = holdout output sample for testing
            
            % Examine how much classifier misclassified approximately of
            % test sample observation
            L = loss(model,inputTest,outputTest, 'LossFun','classiferror')
            
            quality.loss = L;

        end 
        
        function predictedLabels = PredictNBtestSample(obj, model,...
                testIdx, inputTest, outputTest, sampleCount)
            %PREDICTNBTESTSAMPLE Predict labels using NB and test sample
            %   model = model for predicting
            %   testIdx = indices of test sample
            %   inputTest = input of test sample
            %   outputTest = output of test sample
            %   sampleCount = number of random set of observations in the 
            %   test sample
            
            idx = randsample(sum(testIdx),sampleCount);
            label = predict(model,inputTest);
            
            predictedLabels = table(outputTest(idx),label(idx),...
                'VariableNames',{'TrueLabel','PredictedLabel'});
        end
        
        function [label,Posterior,Cost] = predictNB(obj, model, data,...
                boolean)
            %PREDICTNB Predict class using NB-model
            %   model = model that use to predicting
            %   data = predictable data
            %   boolean = using propertie to model or not
            
            if(boolean)
                modelName = getfield(obj, model);
                Mdl = modelName.model;
            else Mdl = model;
            end
            
            [label,Posterior,Cost] = predict(Mdl,data);
        end
        
        
    end
end

