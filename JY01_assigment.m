%Predicting student performance using LMS-data
    %   From data to model, MAT-64010 2019-01 course's Educational data 
    %   mining applications and predicting student performance assignment 
    %   JY01 Merja Halonen

clear all
clc
%% Reading and preprosessing data

case1 = PredictingPerformance('harjoitustyondata.csv','dd.MM.yyyy HH:mm');
time = case1.time;

x_Label = {'Pisteet'};  
y_Label = {'Lukum��r�'};
case1.histogramFigure(case1.data.Pisteet,x_Label, y_Label);

%% Calculate the number of clicks a student has clicked

dataCounts = case1.ClicksCountPerStudent('Pisteet'); 

x_Label = {'Pisteet'};  
y_Label = {'Klikkaukset lkm'};
case1.scatterFigure(dataCounts, 1, 2, x_Label, y_Label);

x_Label = {'Pisteet'};  
y_Label = {'Lukum��r�'};
case1.histogramFigure(case1.point,x_Label, y_Label);

%% Calculate the number of days a student has clicked

dataMat = case1.CalculateDays('data','Pisteet', 'time', dataCounts);

x_Label = {'Pisteet'};  
y_Label = {'P�iv�t'};
case1.scatterFigure(dataMat, 2, 3, x_Label, y_Label);

%% Calculate average clicks per day

dataMat2 = dataMat;
average = case1.CalculateClicksPerDay(dataMat, 2, 3);
dataMat2(:,4) = average;


%% Converts points to grades 1-5 

% Converts points to grades by scaling 
range = [1,5];
gradesScale = case1.ChangePointsToGrades(dataMat, 1, range);
gradesScale(:,2) = dataMat(:,1);

x_Label = {'Arvosanat'};  
y_Label = {'Lukum��r�'};
case1.histogramFigure(gradesScale(:,1),x_Label, y_Label);


% Converts points to grades by egual
gradesEqual = case1.ChangePointsToEqualGrades(dataMat, 1, 5);
gradesEqual(:,2) = dataMat(:,1);

case1.histogramFigure(gradesEqual(:,1),x_Label, y_Label);

% Normal distribution

gradesNorm = case1.ChangePointToNormGrades(6300, 1, dataMat, 5);
case1.histogramFigure(gradesNorm(:,1),x_Label, y_Label);


%% Create input and output vectors which to use in models

% Input and output for scaling grades

[input, outputScale] = case1.CreateInputOutput(dataMat, 2, 3,...
    gradesScale, 1);

x_Label = {'Klikkauksien lkm'};  
y_Label = {'Lukum��r�'};
case1.histogramFigure(input(:,1),x_Label, y_Label);
% 
% figure;
% scatter(input(:,1),input(:,2));

x_Label = {'Klikkaukset lkm'};  
y_Label = {'P�ivien lkm'};
case1.scatterFigure(input, 1, 2, x_Label, y_Label);

% Output for evently distributed grades

[input, outputEqual] = case1.CreateInputOutput(dataMat, 2, 3,...
    gradesEqual, 1);

% Output for normalized distributed grades include 0 grade

[input, outputNorm] = case1.CreateInputOutput(dataMat, 2, 3,...
    gradesNorm, 1);

% Average values included
[inputAverage, outputScale] = case1.CreateInputOutput(dataMat2, 2, 4,...
    gradesScale, 1);

%% Min-max-scaling to input vectors

inputMinMax = case1.MinMaxScaling(input);

inputAverageMinMax = case1.MinMaxScaling(inputAverage);

%% Fit k-nearest neighbor (KNN) classifier and Quality of KNN

% Max number of neighbor
N = 10;

%KNN classifier and quality for inputMinMax and outputScale
case1.ModelsStructure(inputMinMax, outputScale, N, 'modelKNNScale');


%KNN classifier and quality for inputMinMax and outputEqual
case1.ModelsStructure(inputMinMax, outputEqual, N, 'modelKNNEqual');

%KNN classifier and quality for inputMinMax and outputNorm
case1.ModelsStructure(inputMinMax, outputNorm, N, 'modelKNNNorm');

%KNN classifier and quality for inputAverageMinMax and outputScale
case1.ModelsStructure(inputAverageMinMax, outputScale, N,...
'modelKNNScaleAv');

%KNN classifier and quality for inputAverageMinMax and outputEqual
case1.ModelsStructure(inputAverageMinMax, outputEqual, N,...
    'modelKNNEqualAv');

%KNN classifier and quality for inputAverageMinMax and outputEqual
case1.ModelsStructure(inputAverageMinMax, outputNorm, N,...
    'modelsKNNNormAv');

% Save models that can be used those ones again
%save('case1_models.mat','case1', '-v7.3');

load('case1_models.mat');


%% Predicting grade using KNN

% Input which is the predictor data

X = dataMat(:,2:end);
X2 = dataMat2(:,2:end);

% Scores indicating the likelihood that a label comes from a particular 
% class (posterior probabilities). 
% Cost is expected classification cost.

[label, score, cost] = case1.PredictKNN('modelKNNScale', 10, X, input);

[labelSAv, scoreSAv, costSAv] = case1.PredictKNN('modelKNNScaleAv', 8,...
    dataMat2(:,2:end), inputAverage);

[label2, score2, cost2] = case1.PredictKNN('modelKNNScale', 2, X, input);

[labelSAv2, scoreSAv2, costSAv2] = case1.PredictKNN('modelKNNScaleAv', 2,...
    dataMat2(:,2:end), inputAverage);

%% KNN-search
 Y = inputScaleMinMax(1:10,:);
 % Idx is the indices of the nearest neighbors
 % D contains the distances between each observation in Y and the 
 % corresponding closest observations in X.
[Idx,D, neighbors, grades, grade] = case1.SearchKNN(inputScaleMinMax,Y,3,...
    outputScale);


%% Naive Bayes (NB) classification

%Define classNames for modeling
classNamesScale = {'1','2','3','4','5'}; 
classNamesNorm = {'0','1','2','3','4','5'}; 
% Specify that the predictor are multinomial.
distributionScale = 'mn';
distributionNorm = 'normal';
distributionScaleAv = {'mn','mn','mn'};

% Percentage of sample test data
p = 0.30

%sample test size
s = 40;

%NB classifier, quality and labels for sample data for input and outputScale
case1.ModelsStructureNB(input,outputScale,classNamesScale,...
    distributionScale, 'modelNBScale', p, s);

%NB classifier, quality and labels for sample data for input and
%outputNorm
case1.ModelsStructureNB(input,outputNorm,classNamesNorm,...
    distributionNorm,'modelNBNorm', p, s);

%NB classifier, quality and labels for sample data for inputAverage and 
%outputScale. Doesn't work!
case1.ModelsStructureNB(inputAverage,outputScale,classNamesScale,...
    distributionScaleAv, 'modelNBScale', p, s);

%NB classifier, quality and labels for sample data for inputAverage and
%outputNorm. Doesn't work!
case1.ModelsStructureNB(inputAverage,outputNorm,classNamesNorm,...
    distributionNorm,'modelsNBNormAv',p, s);

%save('case1_modelsNB.mat','case1', '-v7.3');

load('case1_modelsNB.mat');

MdlAverageNorm = fitcnb(inputAverage,outputNorm,'ClassNames',...
    classNamesNorm);

% Doesn't work!
MdlAverageScale = fitcnb(inputAverage,outputScale,'ClassNames',...
                    classNamesNorm, 'DistributionNames',distributionScale);
       
lossAverageNorm = loss(MdlAverageNorm, dataMat2(:,2:end), outputNorm,...
    'LossFun','classiferror');
          

% Predicting grades using NB-model

%Label, predicted class labels for the predictor data in the table or data 
%matrix (X), based on the trained, full or compact naive Bayes classifier 
%Mdl.
%Posterior indicating the likelihood that a label comes from a particular 
%class.
%Cost, for each observation in data (X), the predicted class label 
%corresponds to the minimum expected classification costs among all classes.

X = dataMat(:,2:end);
X2 = dataMat2(:,2:end);

[labelNB,PosteriorNB,CostNB] = case1.predictNB('modelNBScale', X, true);

[labelNBnormAv,PosteriorNBnormAv,...
    CostNBnormAv] = case1.predictNB(MdlAverageNorm,X2,false);

%% Regression

% number of clicks
x = dataMat(:,2)
y = dataMat(:,1)
b1 = x\y

yCalc1 = b1*x;
figure;
scatter(x,y)
hold on
plot(x,yCalc1)
xlabel('Klikkauksien lkm')
ylabel('Pisteet')
grid on

%number of days
x2 = dataMat(:,3)

b1 = x2\y

yCalc1 = b1*x2;
figure;
scatter(x2,y)
hold on
plot(x2,yCalc1)
xlabel('P�ivien lkm')
ylabel('Pisteet')
grid on


